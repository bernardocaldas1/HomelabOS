---
site_name: HomelabOS
repo_name: NickBusey/HomelabOS
repo_url: https://gitlab.com/NickBusey/HomelabOS
edit_uri: "/NickBusey/HomelabOS/edit/dev/docs"
markdown_extensions:
  - admonition
  - toc:
      permalink: true
theme:
  name: material
  palette:
    primary: orange
  icon:
    logo: material/home
  features:
    - instant
nav:
- Home: index.md
- Installation: setup/installation.md
- Getting Started: setup/gettingstarted.md
- Setup:
  - Backups: setup/backups.md
  - Bastion Server: setup/bastion.md
  - Email: setup/email.md
  - Traefik: setup/traefik.md
  - Terraform: setup/terraform.md
  - Tor Onion Services: setup/tor.md
  - VPN Setup: setup/vpn.md
- Included Software:
  - Analytics:
    - Matomo: software/matomo.md
  - Automation:
    - Home Assistant: software/homeassistant.md
    - HomeBridge: software/homebridge.md
    - Kibitzr: software/kibitzr.md
  - Blogging Platforms:
    - Ghost: software/ghost.md
  - Calendaring and Contacts Management:
    - NextCloud: software/nextcloud.md
  - Chat:
    - The Lounge: software/thelounge.md
    - Zulip: software/zulip.md
  - Document Management:
    - Mayan: software/mayan.md
    - Paperless: software/paperless.md
  - E-books:
    - Calibre: software/calibre.md
  - Email:
    - Mailserver: software/mailserver.md
    - Mailu: software/mailu.md
  - Federated Identity/Authentication:
    - Authelia: software/authelia.md
    - OpenLDAP: software/openldap.md
  - Feed Readers:
    - FreshRSS: software/freshrss.md
    - Miniflux: software/miniflux.md
  - File Sharing and Synchronization:
    - Duplicati: software/duplicati.md
    - Jackett: software/jackett.md
    - Lidarr: software/lidarr.md
    - Minio: software/minio.md
    - Mylar: software/mylar.md
    - Ombi: software/ombi.md
    - Radarr / Sonarr: software/radarr-sonarr.md
    - SickChill: software/sickchill.md
    - Synchthing: software/syncthing.md
    - Transmission: software/transmission.md
  - Gateways and terminal sharing:
    - Guacamole: software/guacamole.md
    - WebVirtMgr: software/webvirtmgr.md
  - Media Streaming:
    - Airsonic: software/airsonic.md
    - Beets: software/beets.md
    - Emby: software/emby.md
    - Jellyfin: software/jellyfin.md
    - Mstream: software/mstream.md
    - Plex: software/plex.md
  - Misc/Other:
    - Chowdown: software/chowdown.md
    - Darksky: software/darksky.md
    - Dasher: software/dasher.md
    - ERPNext: software/erpnext.md
    - Folding@home: software/folding_at_home.md
    - Grocy: software/grocy.md
    - Grownetics: software/grownetics.md
    - Inventario: software/inventario.md
    - Mashio: software/mashio.md
    - Minecraft: software/minecraft.md
    - Monica: software/monica.md
    - Pi-hole: software/pihole.md
    - Poli: software/poli.md
    - Portainer: software/portainer.md
    - PrivateBin: software/privatebin.md
    - Sabnzbd: software/sabnzbd.md
    - Searx: software/searx.md
    - Ubooquity: software/ubooquity.md
    - Watchtower: software/watchtower.md
  - Money, Budgeting and Management:
    - Firefly III: software/firefly.md
  - Monitoring:
    - Grafana: software/grafana.md
    - HealthChecks: software/healthchecks.md
    - Netdata: software/netdata.md
    - Speedtest: software/speedtest.md
    - Tautulli: software/tautulli.md
    - TICK: software/tick.md
  - Note-taking and Editors:
    - BulletNotes: software/bulletnotes.md
    - Trilium: software/trilium.md
  - Password Managers:
    - Bitwarden: software/bitwarden.md
  - Personal Dashboards:
    - Homedash: software/homedash.md
    - Organizr: software/organizr.md
  - Photo and Video Galleries:
    - Digikam: software/digikam.md
    - OwnPhotos: software/ownphotos.md
    - Piwigo: software/piwigo.md
    - Pixelfed: software/pixelfed.md
  - Read it Later Lists:
    - Wallabag: software/wallabag.md
  - Software Development:
    - Codeserver: software/codeserver.md
    - Drone: software/drone.md
    - Gitea: software/gitea.md
  - Task management/To-do lists:
    - Wekan: software/wekan.md
  - VPN:
    - OpenVPN: software/openvpn.md
  - Webservers:
    - Apache2: software/apache2.md
  - Wikis:
    - Bookstack: software/bookstack.md
- Development:
  - Adding Services: development/adding_services.md
  - Contributing: development/contributing.md
  - Migrations: development/migrations.md
  - Vagrant: development/vagrant.md
